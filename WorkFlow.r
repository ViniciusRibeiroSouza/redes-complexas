# R Script
# Load a package
library(igraph)

library(stringr)

# Reading file
inputFile <- "Datasets/Dataset-Deezer.csv"
dataset <- "Deezer"

# Transforming file to Network
links <- read.csv(inputFile, header=T, as.is=T)
# Examine the data
head(links)
sprintf("Showing Status")
sprintf("Number of links: %i", nrow(links))

# Loading network
net <- graph_from_data_frame(d=links, directed=T) 
class(net)

# printing network data
net

# Todo Improve
# Plotting better
jpeg(file=str_glue("{dataset}_Common-Graph.jpeg"))
plot(net,main=str_glue("Dataset: {dataset} - Common Graph Layout"), edge.arrow.size=.01,vertex.label=NA, vertex.size=2)
dev.off()

# Another plot
jpeg(file=str_glue("{dataset}_Large-Graph.jpeg"))
plot(net, vertex.color="orange", layout=layout_with_lgl, main=str_glue("Dataset: {dataset} - Large Graph Layout"), edge.arrow.size=.01,vertex.label=NA, vertex.size=2)
dev.off()

# Another plot
l <- layout_with_fr(net)
jpeg(file=str_glue("{dataset}_Fruchterman-Reingold.jpeg"))
plot(net, vertex.color="orange", layout=layout_with_lgl, main=str_glue("Dataset: {dataset} - Fruchterman-Reingold Layout"), edge.arrow.size=.01,vertex.label=NA, vertex.size=2)
dev.off()

# 3D sphere layout
l <- layout_with_kk(net)
jpeg(file=str_glue("{dataset}_Kamada-Kawai.jpeg"))
plot(net, layout=l, edge.arrow.size=.01, main=str_glue("Dataset: {dataset} - Kamada-Kawai Layout"), vertex.label=NA, vertex.size=2)
dev.off()

# Transitivity / Clustering
## global - ratio of triangles (direction disregarded) to connected triples.
## local - ratio of triangles to connected triples each vertex is part of.
transitivity(net, type="global")  # net is treated as an undirected network

# Diameter
## A network diameter is the longest geodesic distance (length of the shortest path between two nodes) in the network.
diameter(net, directed=T, weights=NA)

# Density
density_function <- function(network) {
  number_edges <- gsize(network)
  number_nodes <- gorder(network)
  return(2 * number_edges /(number_nodes * (number_nodes-1)))
}

density_function(net)

sprintf("Number of edges: %i", gsize(net))
sprintf("Number of nodes: %i", gorder(net))

# Todo Improve
# Degree
deg <- degree(net, mode="all")
jpeg(file=str_glue("{dataset}_Hist-Degree.jpeg"))
hist(deg, breaks = vcount(net)/10, main="Histogram of node degree", col="darkmagenta", xlab="Degree", xlim = c(0,max(deg)+100))
dev.off()

# Average degree
mean(deg)

deg.dist <- degree_distribution(net, cumulative=T, mode="all")
jpeg(file=str_glue("{dataset}_Prob-Degree.jpeg"))
plot( x=0:max(deg), y=1-deg.dist, pch=19, cex=1.2, col="orange", 
      xlab="Degree", ylab="Cumulative Frequency")
dev.off()

# Distance
mean_distance(net, directed=F)

