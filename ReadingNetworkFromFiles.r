# R Script
# load a package
library(igraph)


# loading the data
nodes <- read.csv("Datasets/Dataset1-Media-Example-NODES.csv", header=T, as.is=T)
links <- read.csv("Datasets/Dataset1-Media-Example-EDGES.csv", header=T, as.is=T)

# Examine the data
head(nodes)

head(links)

nrow(nodes); length(unique(nodes$id))

nrow(links); nrow(unique(links[,c("from", "to")]))

# Aggregating data
links <- aggregate(links[,3], links[,-3], sum)
links <- links[order(links$from, links$to),]
colnames(links)[4] <- "weight"
rownames(links) <- NULL

## Data set 2
nodes2 <- read.csv("Datasets/Dataset2-Media-User-Example-NODES.csv", header=T, as.is=T)
links2 <- read.csv("Datasets/Dataset2-Media-User-Example-EDGES.csv", header=T, row.names=1)

# Examine the data
head(nodes2)
head(links2)
# As adjacency matrix
links2 <- as.matrix(links2)
dim(links2)
dim(nodes2)
