# R Script
# load a package
library(igraph)

# Empty graph
eg <- make_empty_graph(40)
plot(eg, vertex.size = 10, vertex.label = NA)

# Full graph
fg <- make_full_graph(40)
plot(fg, vertex.size = 10, vertex.label = NA)

# Simple star graph
st <- make_star(40)
plot(st, vertex.size = 10, vertex.label = NA)

# Tree graph
tr <- make_tree(40, children = 3, mode = "undirected")
plot(tr, vertex.size = 10, vertex.label = NA)

# Ring graph
rn <- make_ring(40)
plot(rn, vertex.size = 10, vertex.label = NA)

# Erdos-Renyi random graph model
er <- sample_gnm(n = 100, m = 40)
plot(er, vertex.size = 6, vertex.label = NA)

# Watts-Strogatz small-world model
sw <- sample_smallworld(dim = 2, size = 10, nei = 1, p = 0.1)
plot(sw, vertex.size = 6, vertex.label = NA, layout = layout_in_circle)


# Barabasi-Albert model
ba <- sample_pa(n = 100, power = 1, m = 1, directed = F)
plot(ba, vertex.size = 6, vertex.label = NA)

# Historical graphs
zach <- graph("Zachary") # the Zachary carate club
plot(zach, vertex.size = 10, vertex.label = NA)

# Rewire graph to edge endpoints uniformly randomly with a probability prob.
rn.rewired <- rewire(rn, each_edge(prob = 0.1))
plot(rn.rewired, vertex.size = 10, vertex.label = NA)

# Rewire to connect vertices to other vertices at a certain distance.
rn.neigh <- connect.neighborhood(rn, 5)
plot(rn.neigh, vertex.size = 8, vertex.label = NA)

# plotting separated graphs
plot(rn, vertex.size = 10, vertex.label = NA)
plot(tr, vertex.size = 10, vertex.label = NA)

# Combine graphs (disjoint union, assuming separate vertex sets): %du%
plot(rn %du% tr, vertex.size = 10, vertex.label = NA)
