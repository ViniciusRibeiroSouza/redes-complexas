# R Script
# loading lib
library(igraph)

# Plot with curved edges (edge.curved=.1) and reduce arrow size:
plot(net, edge.arrow.size=.4, edge.curved=.1)

plot(net, edge.arrow.size=.2, edge.curved=0,
     vertex.color="orange", vertex.frame.color="#555555",
     vertex.label=V(net)$media, vertex.label.color="black",
     vertex.label.cex=.7)

# Generate colors based on media type:

colrs <- c("gray50", "tomato", "gold")

V(net)$color <- colrs[V(net)$media.type]



# Set node size based on audience size:

V(net)$size <- V(net)$audience.size*0.7



# The labels are currently node IDs.

# Setting them to NA will render no labels:

V(net)$label.color <- "black"

V(net)$label <- NA



# Set edge width based on weight:

E(net)$width <- E(net)$weight/6



#change arrow size and edge color:

E(net)$arrow.size <- .2

E(net)$edge.color <- "gray80"

E(net)$width <- 1+E(net)$weight/12

plot(net)

plot(net, edge.color="orange", vertex.color="gray50") 

plot(net) 

legend(x=-1.5, y=-1.1, c("Newspaper","Television", "Online News"), pch=21,
       col="#777777", pt.bg=colrs, pt.cex=2, cex=.8, bty="n", ncol=1)

# plotting only the labels
plot(net, vertex.shape="none", vertex.label=V(net)$media, 
     vertex.label.font=2, vertex.label.color="gray40",
     vertex.label.cex=.7, edge.color="gray85")

# Set color the edges of the graph based on their source node color
edge.start <- ends(net, es=E(net), names=F)[,1]
edge.col <- V(net)$color[edge.start]
plot(net, edge.color=edge.col, edge.curved=.1)









