# R Script
# loading lib
library(igraph)

dev.off()
# stats about the network
hist(links$weight)
mean(links$weight)
sd(links$weight)

# deleting non relevant nodes
cut.off <- mean(links$weight) 
net.sp <- delete_edges(net, E(net)[weight<cut.off])
plot(net.sp)

# plotting edges color differently
E(net)$width <- 1.5
plot(net, edge.color=c("dark red", "slategrey")[(E(net)$type=="hyperlink")+1],
     vertex.color="gray40", layout=layout.circle)

# plotting edges color differently - second way
net.m <- net - E(net)[E(net)$type=="hyperlink"] # another way to delete edges

net.h <- net - E(net)[E(net)$type=="mention"]

# Plot the two links separately:

par(mfrow=c(1,2))

plot(net.h, vertex.color="orange", main="Tie: Hyperlink")

plot(net.m, vertex.color="lightsteelblue2", main="Tie: Mention")


# Make sure the nodes stay in place in both plots:

l <- layout_with_fr(net)

plot(net.h, vertex.color="orange", layout=l, main="Tie: Hyperlink")

plot(net.m, vertex.color="lightsteelblue2", layout=l, main="Tie: Mention")

dev.off()

# Interactive plotting with tkplot
tkid <- tkplot(net) #tkid is the id of the tkplot that will open

l <- tkplot.getcoords(tkid) # grab the coordinates from tkplot

tk_close(tkid, window.close = T)

plot(net, layout=l)

# Other ways to represent a network
netm <- get.adjacency(net, attr="weight", sparse=F)

colnames(netm) <- V(net)$media

rownames(netm) <- V(net)$media



palf <- colorRampPalette(c("gold", "dark orange")) 

heatmap(netm[,17:1], Rowv = NA, Colv = NA, col = palf(100), 
        
        scale="none", margins=c(10,10) )

# Plotting two-mode networks with igraph
V(net2)$color <- c("steel blue", "orange")[V(net2)$type+1]

V(net2)$shape <- c("square", "circle")[V(net2)$type+1]

V(net2)$label <- ""

V(net2)$label[V(net2)$type==F] <- nodes2$media[V(net2)$type==F] 

V(net2)$label.cex=.4

V(net2)$label.font=2

plot(net2, vertex.label.color="white", vertex.size=(2-V(net2)$type)*8)

# layout for bipartite networks
plot(net2, vertex.label=NA, vertex.size=7, layout=layout_as_bipartite) 

plot(net2, vertex.shape="none", vertex.label=nodes2$media,
     
     vertex.label.color=V(net2)$color, vertex.label.font=2.5, 
     
     vertex.label.cex=.6, edge.color="gray70",  edge.width=2)
