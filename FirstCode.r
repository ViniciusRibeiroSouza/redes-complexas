install.packages("igraph")

# Creating a graph
library(igraph) # Load the igraph package
g4 <- graph(c("John", "Jim", "Jim", "Jack", "Jim", "Jack", "John", "John"),
     isolates = c("Jesse", "Janis", "Jennifer", "Justin")
)

# In named graphs we can specify isolates by providing a list of their names.

plot(g4,
     edge.arrow.size = .5, vertex.color = "gold", vertex.size = 15,
     vertex.frame.color = "gray", vertex.label.color = "black",
     vertex.label.cex = 0.8, vertex.label.dist = 2, edge.curved = 0.2
)

E(g4) # The edges of the object
