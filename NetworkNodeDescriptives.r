# R Script
# loading lib
library(igraph)

# Density
edge_density(net, loops=F)

# For a directed network
ecount(net)/(vcount(net)*(vcount(net)-1))

# Reciprocity
reciprocity(net)

dyad_census(net) # Mutual, asymmetric, and nyll node pairs

2*dyad_census(net)$mut/ecount(net) # Calculating reciprocity

# Transitivity
## global - ratio of triangles (direction disregarded) to connected triples.
## local - ratio of triangles to connected triples each vertex is part of.

transitivity(net, type="global")  # net is treated as an undirected network

transitivity(as.undirected(net, mode="collapse")) # same as above

transitivity(net, type="local")

triad_census(net) # for directed networks

# Diameter
## A network diameter is the longest geodesic distance (length of the shortest path between two nodes) in the network.
diameter(net, directed=F, weights=NA)

diameter(net, directed=F)

diam <- get_diameter(net, directed=T)

diam

class(diam)
as.vector(diam)

# Color nodes along the diameter
vcol <- rep("gray40", vcount(net))
vcol[diam] <- "gold"
ecol <- rep("gray80", ecount(net))
ecol[E(net, path=diam)] <- "orange" 

# E(net, path=diam) finds edges along a path, here 'diam'

plot(net, vertex.color=vcol, edge.color=ecol, edge.arrow.mode=0)

# Node Degrees
deg <- degree(net, mode="all")

plot(net, vertex.size=deg*3)

hist(deg, breaks=1:vcount(net)-1, main="Histogram of node degree")

# Degree distribution
deg.dist <- degree_distribution(net, cumulative=T, mode="all")
plot( x=0:max(deg), y=1-deg.dist, pch=19, cex=1.2, col="orange", 
      xlab="Degree", ylab="Cumulative Frequency")

component_distribution(net, cumulative = FALSE, mul.size = FALSE)
components(net, mode = c("weak", "strong"))
