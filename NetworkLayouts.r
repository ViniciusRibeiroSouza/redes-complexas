# R Script
# loading lib
library(igraph)

# Network layouts are simply algorithms that return coordinates for each node in a network.
net.bg <- sample_pa(80) 

V(net.bg)$size <- 8

V(net.bg)$frame.color <- "white"

V(net.bg)$color <- "orange"

V(net.bg)$label <- "" 

E(net.bg)$arrow.mode <- 0

plot(net.bg)

# setting layout display
plot(net.bg, layout=layout_randomly)
l <- layout_in_circle(net.bg)
plot(net.bg, layout=l)

# custom layout
l <- cbind(1:vcount(net.bg), c(1, vcount(net.bg):2))
plot(net.bg, layout=l)

# Randomly placed vertices
l <- layout_randomly(net.bg)
plot(net.bg, layout=l)

# Circle layout
l <- layout_in_circle(net.bg)
plot(net.bg, layout=l)

# 3D sphere layout
l <- layout_on_sphere(net.bg)
plot(net.bg, layout=l)

# Fruchterman-Reingold is one of the most used force-directed layout algorithms out there.
l <- layout_with_fr(net.bg)
plot(net.bg, layout=l)

# Multiple figures
par(mfrow=c(2,2), mar=c(0,0,0,0))   # plot four figures - 2 rows, 2 columns
plot(net.bg, layout=layout_with_fr)
plot(net.bg, layout=layout_with_fr)
plot(net.bg, layout=l)
plot(net.bg, layout=l)

dev.off()

# Using rescale on graphs
l <- layout_with_fr(net.bg)
l <- norm_coords(l, ymin=-1, ymax=1, xmin=-1, xmax=1)

par(mfrow=c(2,2), mar=c(0,0,0,0))
plot(net.bg, rescale=F, layout=l*0.4)
plot(net.bg, rescale=F, layout=l*0.6)
plot(net.bg, rescale=F, layout=l*0.8)
plot(net.bg, rescale=F, layout=l*1.0)

dev.off()

# Kamada Kawai - Force-directed algorithm
l <- layout_with_kk(net.bg)
plot(net.bg, layout=l)

# The LGL algorithm is meant for large, connected graphs
plot(net.bg, layout=layout_with_lgl)

# All available layouts in igraph
layouts <- grep("^layout_", ls("package:igraph"), value=TRUE)[-1] 

# Remove layouts that do not apply to our graph.

layouts <- layouts[!grepl("bipartite|merge|norm|sugiyama|tree", layouts)]

par(mfrow=c(3,3), mar=c(1,1,1,1))

for (layout in layouts) {
  
  print(layout)
  
  l <- do.call(layout, list(net)) 
  
  plot(net, edge.arrow.mode=0, layout=l, main=layout) }
