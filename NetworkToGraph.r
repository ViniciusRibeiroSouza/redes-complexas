# R Script
# loading lib
library(igraph)

# loading network
net <- graph_from_data_frame(d=links, vertices=nodes, directed=T) 
class(net)
# summary of the network
net

# network data
E(net)       # The edges of the "net" object

V(net)       # The vertices of the "net" object

E(net)$type  # Edge attribute "type"

V(net)$media # Vertex attribute "media"

# plotting network data
plot(net, edge.arrow.size=.4,vertex.label=NA)

# removing loops in the graph
net <- simplify(net, remove.multiple = F, remove.loops = T)

# extracting data
as_edgelist(net, names=T)
as_adjacency_matrix(net, attr="weight")

# data frames describing nodes and edges
as_data_frame(net, what="edges")

as_data_frame(net, what="vertices")

head(nodes2)
head(links2)

net2 <- graph_from_incidence_matrix(links2)
table(V(net2)$type)

# bipartite projection
net2.bp <- bipartite.projection(net2)

plot(net2.bp$proj1, vertex.label.color="black", vertex.label.dist=1,
     vertex.size=7, vertex.label=nodes2$media[!is.na(nodes2$media.type)])

plot(net2.bp$proj2, vertex.label.color="black", vertex.label.dist=1,
     vertex.size=7, vertex.label=nodes2$media[ is.na(nodes2$media.type)])
